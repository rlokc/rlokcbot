FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /source

COPY *.sln .

COPY DiscordBot/*.csproj ./DiscordBot/
RUN dotnet restore ./DiscordBot/DiscordBot.csproj

COPY DiscordBot/. ./DiscordBot/
RUN cd /source/DiscordBot
RUN dotnet publish -c release -o /bot DiscordBot

FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /bot
COPY --from=build /bot/ /bot/.
ENTRYPOINT ["./DiscordBot"]
#ENTRYPOINT ["/bin/bash"]

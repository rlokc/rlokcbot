using System;
using DiscordBot.Services;
using DiscordBot.Util;
using Serilog.Debugging;
using Xunit;

namespace DiscordBot.Tests
{
    public class DateTimeParserTests
    {
        private DateTimeParser _parser;
        private DateTime _now = new DateTime(2021, 03, 20, 5, 0, 0, DateTimeKind.Utc);
        public DateTimeParserTests()
        {
            this._parser = new DateTimeParser();
            
            this._parser.NowProvider = () => this._now;
        }

        [Theory]
        [InlineData("сегодня", "2021-03-20")]
        [InlineData("завтра", "2021-03-21")]
        [InlineData("15 июня", "2021-06-15")]
        [InlineData("20 марта", "2021-03-20")]
        [InlineData("21 марта", "2021-03-21")]
        [InlineData("19 марта", "2022-03-19")]
        [InlineData("2 февраля", "2022-02-02")]
        public void TestDateParsing(string inData, string expected)
        {
            var expected_date = DateTime.ParseExact(expected, "yyyy-MM-dd", null);
            var actual = this._parser.Parse(inData);
            Assert.Equal(expected_date.Date, actual.Date);
        }

        [Theory]
        // This one doesn't work because DateTime.Parse() uses inbuilt DateTime.Now
        // That we can't stub out. Oops
        // [InlineData("15:35", "2021-03-20 15:35:00")]
        [InlineData("сегодня", "2021-03-20 20:00:00")]
        [InlineData("сегодня 19:00", "2021-03-20 19:00:00")]
        [InlineData("15 марта", "2022-03-15 20:00:00")]
        [InlineData("15 марта 15:00", "2022-03-15 15:00:00")]
        [InlineData("20 июля", "2021-07-20 20:00:00")]
        [InlineData("20 июля 15:30", "2021-07-20 15:30:00")]
        [InlineData("20 июля 2022", "2022-07-20 20:00:00")]
        [InlineData("20 июля 2022 15:30", "2022-07-20 15:30:00")]
        [InlineData("2022-04-20", "2022-04-20 20:00:00")]
        [InlineData("2022-04-20 15:20", "2022-04-20 15:20:00")]
        public void TestDateTimeParsing(string inData, string expected)
        {
            var expected_date = DateTime.ParseExact(expected, "yyyy-MM-dd HH:mm:ss", null);
            expected_date = TimeZoneInfo.ConvertTimeToUtc(expected_date, DateUtil.MoscowTimeZone);
            var actual = this._parser.Parse(inData);
            Assert.Equal(expected_date, actual);
        }
    }
}
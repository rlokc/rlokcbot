using System.Threading.Tasks;
using Discord.Commands;

namespace DiscordBot.Bot.Discord.Modules
{
    public class TrelloModule : ModuleBase<SocketCommandContext>
    {
        private static string BOARDLINK = "https://trello.com/invite/b/pzZSdrmI/19de3e2577857656d5f946b261d48a00/rlokc-discord-bot";

        [Command("request")]
        public Task RequestFeature()
        {
            var s = $"Можно запросить новые фичи по ссылке:\r\n{BOARDLINK}";
            return ReplyAsync(s);
        }
    }
}
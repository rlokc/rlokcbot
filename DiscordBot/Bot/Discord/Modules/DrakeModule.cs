using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace DiscordBot.Bot.Discord.Modules 
{
    public class DrakeModule 
    {
        private static readonly Regex GoVRegex = new Regex(@"^Го в\b", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        public DrakeModule()
        {

        }

        private const string DrakeYesStr = "<:drake_yes:384775155341066240>";
        private const string DrakeNoStr = "<:drake_no:384775155311837188>";

        public async Task MessageReceived(SocketMessage message)
        {
            if (message.Author.IsBot)
            {
                return;
            }

            var match = GoVRegex.Match(message.Content);
            if (match.Success)
            {
                var emotes = new IEmote[] {
                    Emote.Parse(DrakeYesStr),
                    Emote.Parse(DrakeNoStr),
                };
                await Task.WhenAll(emotes.Select(e => message.AddReactionAsync(e)));
            }
        }
    }
}

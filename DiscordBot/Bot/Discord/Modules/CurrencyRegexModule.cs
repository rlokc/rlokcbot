using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using DiscordBot.Services;
using DiscordBot.Util;

namespace DiscordBot.Bot.Discord.Modules
{
    public class CurrencyRegexModule
    {
        private readonly ICurrencyConverter _converter;

        private static readonly Regex CurrencyRegex = new Regex(@"\d+([.,]\d+)?[mkмк]? +[A-Za-z]{3}\b", 
            RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);



        public CurrencyRegexModule(ICurrencyConverter converter)
        {
            _converter = converter ?? throw new ArgumentNullException("converter");
        }

        public async Task MessageReceived(SocketMessage message)
        {
            if (message.Author.IsBot)
            {
                return;
            }
            var match = CurrencyRegex.Match(message.Content);
            if (match.Success)
            {
                try
                {
                    var response = await _converter.Convert(DiscordUtil.GetGuildId(message), match.Value);
                    if (response != null)
                        await message.Channel.SendMessageAsync(embed: genEmbed(response));
                }
                catch (Exception ex)
                {
                    Serilog.Log.Warning(ex.ToString());
                }
            }
        }

        private Embed genEmbed(IDictionary<string, decimal> converted) 
        {
            var builder = new EmbedBuilder();

            var stringed = converted
                .OrderBy(k => k.Key)
                .Select(kvp => new KeyValuePair<string, string>(kvp.Key, kvp.Value.ToString("n2")))
                .ToList();

            var maxNumLen = stringed.Max(kvp => kvp.Value.Length);

            var sb = new StringBuilder();
            sb.Append("```");
            foreach (var kvp in stringed)
            {
                sb.Append(kvp.Key + "\t");
                sb.Append(kvp.Value.PadLeft(maxNumLen));
                sb.Append("\r\n");
            }
            sb.Append("```");

            builder.AddField("\u200b", sb.ToString());

            builder.WithDescription("Перевод валюты");

            return builder.Build();
        }
    }
}
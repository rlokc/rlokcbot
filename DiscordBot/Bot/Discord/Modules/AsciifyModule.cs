using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace DiscordBot.Bot.Discord.Modules
{

    [NamedArgumentType]
    public class AsciifyArguments 
    {
        public int W { get; set; } = 40;
    }

    public class AsciifyModule : ModuleBase<SocketCommandContext>
    {
        public AsciifyModule()
        {

        }

        [Command("ascii")]
        public async Task Ascii_Help()
        {
            await ReplyAsync("`~ascii_photo %url%` для фото\r\n"+
                             "`~ascii_emote %url%` для эмоутов (плоские цвета)\r\n"+
                             "Стоит попробовать другой тип, если первый не понравился, они по разному работают\r\n"+
                             "Добавьте invert после ссылки, чтобы инвертировать результат");
        }

        const int ASCII_WIDTH = 40;
        const float ASCII_ASPECT_RATIO = 1.7f;
        const float BINARIZATION_THRESHOLD = 0.4f;
        const int WHITE_PIXEL_THRESHOLD = 100;

        [Command("ascii_photo")]
        public async Task Asciify_photo(string imgRawUri, [Remainder] string rest = "")
        {
            await Asciify(imgRawUri, rest, x => x.DetectEdges());
        }

        [Command("ascii_emote")]
        public async Task Asciify_emote(string imgRawUri, [Remainder] string rest = "")
        {
            await Asciify(imgRawUri, rest, x => x.BinaryThreshold(BINARIZATION_THRESHOLD));
        }

        public async Task Asciify(string imgRawUri, string restArgs, Action<IImageProcessingContext> edgeDetectionFunc)
        {
            var imgUri = new Uri(imgRawUri);
            byte[] imgBytes;
            int width = ASCII_WIDTH;
            using (var webClient = new WebClient())
            {
                webClient.DownloadData(imgUri);
                imgBytes = await webClient.DownloadDataTaskAsync(imgUri);
            }
            using (var image = SixLabors.ImageSharp.Image.Load(imgBytes))
            {
                var aspect = (double) image.Bounds().Width / image.Bounds().Height;
                // Make the height smaller because the text chars are very tall
                aspect *= ASCII_ASPECT_RATIO;
                var height = (int)(width / aspect);
                // Multiplying them by the resolution of our braille symbols
                var size = new Size(width*2, height*4);

                var resImg = image
                    .CloneAs<SixLabors.ImageSharp.PixelFormats.L8>()
                    .Clone(x => x.Resize(size))
                    .Clone(edgeDetectionFunc);
                if (restArgs.Contains("invert", StringComparison.CurrentCultureIgnoreCase))
                {
                    resImg = resImg.Clone(x => x.Invert());
                }

                // For debugging: output image
                // var outputStream = new MemoryStream();
                // await resImg.SaveAsJpegAsync(outputStream);
                // outputStream.Seek(0, SeekOrigin.Begin);
                // await Context.Channel.SendFileAsync(outputStream, "file.jpg");


                // Generating the ascii art
                var res = new StringBuilder();
                res.Append("```");
                // Dictionary of transformation for the braille symbol
                // Braile dots are laid out like this:
                //0 3
                //1 4
                //2 5
                //6 7
                Dictionary<int, (int x, int y)> transform = new Dictionary<int, (int x, int y)> {
                    {0, (0, 0)},
                    {1, (0, 1)},
                    {2, (0, 2)},
                    {3, (1, 0)},
                    {4, (1, 1)},
                    {5, (1, 2)},
                    {6, (0, 3)},
                    {7, (1, 3)},
                };
                for (int i = 0; i < size.Height / 4; i++) {
                    for (int j = 0; j < size.Width / 2; j++) {
                        // Get 8 pixels around the point we're looking at
                        byte charVal = 0;
                        for (byte k = 0; k < 8; k++) {
                            var pixelX = j * 2 + transform[k].x;
                            var pixelY = i * 4 + transform[k].y;
                            var pixelVal = resImg[pixelX, pixelY].PackedValue;
                            if (pixelVal > WHITE_PIXEL_THRESHOLD)
                            {
                                charVal ^= (byte)(1 << k);
                            }
                        }
                        // Transform it into a braille char (add the byte value to 0x2800 (which is the empty braille symbol))
                        var resChar = char.ConvertFromUtf32(0x2800 + charVal);
                        res.Append(resChar);
                    }
                    res.AppendLine();
                }
                res.Append("```");
                await ReplyAsync(res.ToString());
            }
        }
    }
}
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using DiscordBot.Bot.Discord.Data;
using DiscordBot.Bot.Discord.Data.Entities;
using DiscordBot.Services;
using Microsoft.VisualBasic;

namespace DiscordBot.Bot.Discord.Modules
{
    public class CurrencyModule : ModuleBase<SocketCommandContext>
    {

        private readonly ICurrencyConverter _converter;
        private readonly IRepository<CurrencyConvertorList> _repository;

        public CurrencyModule(ICurrencyConverter converter, IRepository<CurrencyConvertorList> repository)
        {
            _converter = converter ?? throw new ArgumentNullException(nameof(converter));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        private static string DOCSTRING = "Использование:\r\n\t~convert *количество* *валюта*" +
                                          "\r\nКонвертирует заданную валюту сразу в следующие - USD, RUB, BYN, UAH"+
                                          "\r\nПример: ~convert 400 BYN";


        [Command("convert")]
        public async Task Convert([Remainder] string args = "")
        {
            if (string.IsNullOrWhiteSpace(args))
            {
                await ReplyAsync(DOCSTRING);
                return;
            }
            try
            {
                var converted = await _converter.Convert(this.Context.Guild.Id, args);
                if (converted == null)
                {
                    return;
                }

                var resBuilder = new StringBuilder();
                foreach(var kvp in converted.OrderBy(kvp => kvp.Key))
                {
                    resBuilder.AppendLine($"{kvp.Key}: {kvp.Value.ToString("N2")}");
                }

                await ReplyAsync(resBuilder.ToString());
            }
            catch (Exception ex)
            {
                await ReplyAsync($"{ex.Message}\r\n{DOCSTRING}");
            }
        }

        [Command("addcurrency")]
        [RequireUserPermission(GuildPermission.ModerateMembers)]
        public async Task AddCurrencies([Remainder] string args = "")
        {
            if (string.IsNullOrWhiteSpace(args))
            {
                await ReplyAsync("Введите валюты (Код из 3 букв)");
                return;
            }

            var currencies = args
                .Split()
                .Where(CurrencyConvertorList.IsValidCurrency)
                .ToList();

            try
            {
                var lists = await _repository.GetAll();
                var list = lists.FirstOrDefault(l => l.GuildId == Context.Guild.Id) 
                           ?? new CurrencyConvertorList(Context.Guild.Id, CurrencyConvertorList.DefaulCurrencies);

                foreach (var cur in currencies)
                {
                    list.AddCurrency(cur);
                }

                await _repository.Delete(list);
                await _repository.Insert(list);
                await ReplyAsync($"Добавил валюты: {Strings.Join(currencies.ToArray(), ", ")}");
            }
            catch (Exception ex)
            {
                await ReplyAsync($"Ошибка при добавлении валюты: {ex.Message}");
            }
        }
        
        [Command("removecurrency")]
        [RequireUserPermission(GuildPermission.ModerateMembers)]
        public async Task RemoveCurrencies([Remainder] string args = "")
        {
            if (string.IsNullOrWhiteSpace(args))
            {
                await ReplyAsync("Введите валюты (Код из 3 букв)");
                return;
            }

            var currencies = args
                .Split()
                .Where(CurrencyConvertorList.IsValidCurrency)
                .ToList();

            try
            {
                var lists = await _repository.GetAll();
                var list = lists.FirstOrDefault(l => l.GuildId == Context.Guild.Id) 
                           ?? new CurrencyConvertorList(Context.Guild.Id, CurrencyConvertorList.DefaulCurrencies);

                foreach (var cur in currencies)
                {
                    list.RemoveCurrency(cur);
                }

                await _repository.Delete(list);
                await _repository.Insert(list);
                await ReplyAsync($"Удалил валюты: {Strings.Join(currencies.ToArray(), ", ")}");
            }
            catch (Exception ex)
            {
                await ReplyAsync($"Ошибка при удалении валюты: {ex.Message}");
            }
        }
    }
}
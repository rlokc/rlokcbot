using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using DiscordBot.Bot.Discord.Data;
using DiscordBot.Bot.Discord.Data.Entities;
using DiscordBot.Services;
using DiscordBot.Util;
using Serilog;

namespace DiscordBot.Bot.Discord.Modules
{
    public class RemindMeModule : ModuleBase<SocketCommandContext>
    {
        private readonly IRepository<RemindMeEntity> _repository;
        private readonly DiscordBot _bot;
        private readonly IReminderService _reminderService;
        private readonly IDateTimeParser _dtParser;
        public RemindMeModule(IRepository<RemindMeEntity> repository,
                              IReminderService reminderService,
                              IDateTimeParser dtParser)
        {
            _repository = repository;
            _bot = Program.BotInstance;
            _reminderService = reminderService;
            _dtParser = dtParser;
        }

        private static string DOCSTRING = "Использование:\r\n\t~remindme %дата/интервал% - текст уведомления" +
                                          "\r\nПример:\r\n\t~remindme завтра - пукнуть"+
                                          "\r\n\t~remindme 20:00 - уведомление на сегодня"+
                                          "\r\n\t~remindme 15 июля - уведомление на 15 июля 20:00 по МСК"+
                                          "\r\n\t~remindme 15 июля 21:15 - уведомление с точным временем"+
                                          "\r\n\r\nИспользуйте ~reminders, чтобы посмотреть свои уведомления";

        [Command("remindme")]
        public Task RemindMe([Remainder]string rawargs = "")
        {
            if (string.IsNullOrWhiteSpace(rawargs) || 
                rawargs.Trim().Equals("help", StringComparison.InvariantCultureIgnoreCase))
            {
                return ReplyAsync(DOCSTRING);
            }
            var args = rawargs.Split('-');
            var dateRaw = args[0];
            var caption = string.Join('-', args.Skip(1)).Trim();
            DateTime date = DateTime.Now;
            try 
            {
                date = _dtParser.Parse(dateRaw);
            }
            catch (Exception ex)
            {
                return ReplyAsync(ex.Message);
            }
            
            var reminder = new RemindMeEntity()
            {
                ChannelId = this.Context.Channel.Id,
                UserId = this.Context.User.Id,
                ReminderDate = date,
                ReminderCaption = caption,
            };

            _reminderService.AddReminder(reminder);

            var reminderDateInMSK = TimeZoneInfo.ConvertTimeFromUtc(reminder.ReminderDate, DateUtil.MoscowTimeZone);
            return ReplyAsync($"Добавил напоминание \"{caption}\" на {reminderDateInMSK.ToString("f", DateUtil.DATELOCALE)}");
        }

        [Command("reminders")]
        public Task Reminders(string param = "")
        {
            var res = "";
            bool showEveryone = param.Contains("-all", StringComparison.InvariantCultureIgnoreCase);
            if (_reminderService.Reminders?.Count == 0)
            {
                return ReplyAsync("Напоминаний нет");
            }
            if (showEveryone)
            {
                res = "Напоминания всех пользователей:\r\n";
                res += getRemindersString(_reminderService.Reminders, true);
            }
            else
            {
                res = $"Напоминания пользователя {Context.User.Username}:\r\n";
                res += getRemindersString(_reminderService.Reminders.Where(r => r.UserId == Context.User.Id));
            }
            Log.Information(res);
            return ReplyAsync(res);
        }

        private string getRemindersString(IEnumerable<RemindMeEntity> reminders, bool showUser = false)
        {
            var res = "";
            int i = 1;
            foreach (var reminder in reminders)
            {
                var user = Context.Client.GetUser(reminder.UserId);
                res += $"{i}: ";
                if (showUser)
                    res += $"{user?.Username ?? "No user"}: ";
                var localizedDate = TimeZoneInfo.ConvertTimeFromUtc(reminder.ReminderDate, DateUtil.MoscowTimeZone);
                res += $"\"{reminder.ReminderCaption}\", {localizedDate.ToString("f", DateUtil.DATELOCALE)}\r\n";
                i++;
            }
            return res;
        }
    }
}
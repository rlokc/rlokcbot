using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord.WebSocket;
using DiscordBot.Bot.Discord.Data.Entities;
using DiscordBot.Services;

namespace DiscordBot.Bot.Discord.Modules
{
    public class RegexMatchModule
    {

        private readonly Random _random;

        private List<RegexMatch> _matches;

        private const string DUSHNYARA_PASTA = "какой ж ты душняра, ёп твою мааать.. Уже третьи лица сказали "+
        "что ты действительно какой то лол говна. Какая хуй разница "+
        "как это правильно выводитсяназываетсяработает? "+
        "Пиздец, блять... Выдаешь шутку - приходит сраная душила, "+
        "которая начинает выёбываться о неправильности определений. "+
        "Пошла на хуй отсюда, если мои шутки не нравятся - молчи, "+
        "и позволь мне побыть тупым в твоём богатом на знания "+
        "внутреннем мире! Дешевка ебучая, которая триггерится на шутейки, "+
        "я ебал....";

        public RegexMatchModule(IRandomService random)
        {
            _random = random.Random;
            _matches = new List<RegexMatch>();
            _matches.Add(new RegexMatch(@"\b(да|da)\W*$", 0.03f, "Пизда"));
            _matches.Add(new RegexMatch(@"\b(нет|net)\W*$", 0.03f, "Хуем в твой еблет, блядь"));
            _matches.Add(new RegexMatch(@"\b(мех|meh)\W*$", 0.03f, "Шубой оберни орех, уебан"));
            _matches.Add(new RegexMatch(@"\b(не)\W*$", 0.03f, "Хуем в твой ебле, блядь"));
            _matches.Add(new RegexMatch(@"^какой ж(е)? ты.*", 0.5f, DUSHNYARA_PASTA));
            _matches.Add(new RegexMatch(@".*душняра.*", 0.15f, DUSHNYARA_PASTA));
            _matches.Add(new RegexMatch(@"\b(ок|ok)\W*$", 0.03f, "Sosi cock"));
            _matches.Add(new RegexMatch(@"\b(пиздец)\W*$", 0.10f, "Дом с песком"));
        }

        public async Task MessageReceived(SocketMessage message)
        {
            if (message.Author.IsBot)
            {
                return;
            }
            foreach (var match in _matches)
            {
                await SendResponseIfMatched(message, match);
            }
        }

        public async Task SendResponseIfMatched(SocketMessage message, RegexMatch match)
        {
            if (!match.Regex.IsMatch(message.Content))
            {
                return;
            }
            if (_random.NextDouble() < match.Chance)
            {
                await message.Channel.SendMessageAsync(match.Response);
            }
        }
    }
}

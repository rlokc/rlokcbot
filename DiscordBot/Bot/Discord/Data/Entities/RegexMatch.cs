using System.Text.RegularExpressions;

namespace DiscordBot.Bot.Discord.Data.Entities
{
    public class RegexMatch
    {

        public const RegexOptions DefaultOptions = RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant;


        public long Id { get; set; }
        public Regex Regex { get; private set; }
        public float Chance { get; private set; }
        public string Response { get; private set; }

        public RegexMatch(string regexString, float chance, string response)
        : this(new Regex(regexString, DefaultOptions), chance, response) {}

        public RegexMatch(Regex regex, float chance, string response)
        {
            Regex = regex;
            Chance = chance;
            Response = response;
        }

    }
}
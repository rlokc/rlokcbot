using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DiscordBot.Bot.Discord.Data.Entities;

public class CurrencyConvertorList
{

    private const string SEPARATOR = ";";
    private static Regex CurrencyRegex = new Regex("^[a-z]{3}$", RegexOptions.IgnoreCase);

    public static IEnumerable<string> DefaulCurrencies
    {
        get
        {
            return new string[]
            {
                "RUB",
                "USD",
                "EUR",
                "UAH",
            };
        }
    }



    public ulong GuildId { get; set; }
    public List<string> Currencies { get; private set; }

    public CurrencyConvertorList(ulong guildId, IEnumerable<string> currencies)
    {
        GuildId = guildId;
        Currencies = currencies.ToList();
    }

    public CurrencyConvertorList(ulong guildId, string currencies) 
        : this(guildId, ParseCurrencyString(currencies)) {}

    public CurrencyConvertorList(string guildId, string currencies)
        : this(ulong.Parse(guildId), currencies) {}


    public string CurrenciesToString()
    {
        return String.Join(SEPARATOR, this.Currencies);
    }

    public static IEnumerable<string> ParseCurrencyString(string currencies)
    {
        return currencies.Split(SEPARATOR);
    }

    public void AddCurrency(string currency)
    {
        if (!IsValidCurrency(currency))
        {
            throw new ArgumentException(nameof(currency));
        }
        Currencies.Add(currency.ToUpper()); 
    }

    public void RemoveCurrency(string currency)
    {
        if (!IsValidCurrency(currency))
        {
            throw new ArgumentException(nameof(currency));
        }

        Currencies.Remove(currency.ToUpper());
    }

    public static bool IsValidCurrency(string currency)
    {
        return CurrencyRegex.IsMatch(currency);
    }
}
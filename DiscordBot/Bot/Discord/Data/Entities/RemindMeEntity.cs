using System;

namespace DiscordBot.Bot.Discord.Data.Entities
{
    public class RemindMeEntity
    {
        public long Id { get; set; }
        public ulong ChannelId { get; set; }
        public ulong UserId { get; set; }
        public DateTime ReminderDate { get; set; }
        public string ReminderCaption { get; set; }
        public bool IsSent { get; set; } = false;
    }
}
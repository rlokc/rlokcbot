using System.Data.SQLite;

namespace DiscordBot.Bot.Discord.Data
{
    public class SQLUtil 
    {
        public static void CreateDBIfNotExists(string connectionString)
        {
            using (var conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                if (!IsDbExists(conn))
                    CreateDb(conn);
            }
        }

        private static bool IsDbExists(SQLiteConnection conn)
        {
            using (var comm = new SQLiteCommand(conn))
            {
                comm.CommandText = "select count(*) from a";
                try 
                {
                    using (var reader = comm.ExecuteReader())
                    {
                        reader.Read();
                        return (long)reader[0] != 0;
                    }
                }
                catch (SQLiteException)
                {
                    return false;
                }
            }
        }

        private static void CreateDb(SQLiteConnection conn)
        {
            using (var cmd = new SQLiteCommand(conn))
            {
                cmd.CommandText = "create table if not exists a (id integer not null primary key autoincrement)";
                cmd.ExecuteNonQuery();
            }
            using (var cmd = new SQLiteCommand(conn))
            {
                cmd.CommandText = "insert into a default values";
                cmd.ExecuteNonQuery();
            }
        }
    }
}
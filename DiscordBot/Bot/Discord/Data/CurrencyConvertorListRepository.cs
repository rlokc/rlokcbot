using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;
using Dapper;
using DiscordBot.Bot.Discord.Data.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Serilog;

namespace DiscordBot.Bot.Discord.Data;

public class CurrencyConvertorListRepository : IRepository<CurrencyConvertorList>
{

    private readonly string _connectionString;
    private readonly string _tableName = "currency_lists";
    private IEnumerable<CurrencyConvertorList> _cache;

    public CurrencyConvertorListRepository(IConfiguration configuration)
    {
        _connectionString = configuration.GetConnectionString("SQLiteFile");
        _cache = null;
    }

    public void CreateTableIfNotExists()
    {
        Log.Debug("Creating table {TableName}", _tableName);
        using var conn = new SQLiteConnection(_connectionString);
        conn.Open();
        var query = $@"create table if not exists
                            {_tableName}
                            (guildId text not null primary key,
                            currencies text
                            )";
        conn.Execute(query);
    }

    public async Task<IEnumerable<CurrencyConvertorList>> GetAll()
    {
        if (_cache == null)
        {
            _cache = await RefreshCache();
        }

        return _cache;
    }

    private async Task<IEnumerable<CurrencyConvertorList>> RefreshCache()
    {
        await using var conn = new SQLiteConnection(_connectionString);
        await conn.OpenAsync();
        var query = $"select * from {_tableName}";
        return await conn.QueryAsync<CurrencyConvertorList>(query);
    }

    public async Task<CurrencyConvertorList> Insert(CurrencyConvertorList value)
    {
        await using var conn = new SQLiteConnection(_connectionString);
        var query = $@"insert into {_tableName}
                       values (@guildId, @currencies)";
        var valObj = new
        {
            @guildId = value.GuildId,
            @currencies = value.CurrenciesToString()
        };
        await conn.ExecuteAsync(query, valObj);
        _cache = await RefreshCache();
        return value;
    }

    public async Task Delete(ulong id)
    {
        await using var conn = new SQLiteConnection(_connectionString);
        var query = $@"delete from {_tableName} where guildId=@GuildId";
        var valObj = new { @GuildId = id };
        _cache = await RefreshCache();
        await conn.ExecuteAsync(query, valObj);
    }

    public async Task Delete(CurrencyConvertorList value)
    {
        await Delete(value.GuildId);
    }
}
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;
using Dapper;
using DiscordBot.Bot.Discord.Data.Entities;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace DiscordBot.Bot.Discord.Data
{
    public class RegexReplyRepository : IRepository<RegexMatch>
    {
        private readonly string _connectionString;

        private readonly string _tableName = "regex_replies";

        public RegexReplyRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("SQLiteFile");
        }

        public async Task<IEnumerable<RegexMatch>> GetAll()
        {
            await using var conn = new SQLiteConnection(_connectionString);
            await conn.OpenAsync();
            var query = $"select * from {_tableName}";
            return await conn.QueryAsync<RegexMatch>(query);
        }

        public async Task<RegexMatch> Insert(RegexMatch value)
        {
            await using var conn = new SQLiteConnection(_connectionString);
            var query = $@"insert into {_tableName} (regex, response, chance)
                               values (@regex, @response, @chance)";
            var valObj = new {
                @regex = value.Regex.ToString(),
                @response = value.Response,
                @chance = value.Chance
            };

            await conn.ExecuteAsync(query, valObj);
            return value;
        }

        public async Task Delete(ulong id)
        {
            await using var conn = new SQLiteConnection(_connectionString);
            var query = $@"delete from {_tableName} where Id = @Id";
            var valObj = new { @Id = id };
            await conn.ExecuteAsync(query, valObj);
        }

        public async Task Delete(RegexMatch value)
        {
            await Delete((ulong)value.Id);
        }

        public void CreateTableIfNotExists()
        {
            Log.Debug($"Creating table {_tableName}");
            using var conn = new SQLiteConnection(_connectionString);
            conn.Open();
            var query = $@"create table if not exists
                                    {_tableName}
                                    (Id integer not null primary key autoincrement,
                                    regex text,
                                    response text,
                                    chance real)
                                    ";
            conn.Execute(query);
        }
    }
}
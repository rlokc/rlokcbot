using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;
using Dapper;
using DiscordBot.Bot.Discord.Data.Entities;
using DiscordBot.Util;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace DiscordBot.Bot.Discord.Data
{
    public class RemaindersRepository : IRepository<RemindMeEntity>
    {
        private IConfiguration _config;
        private string _connectionString;
        public RemaindersRepository(IConfiguration configuration)
        {
            _config = configuration;
            _connectionString = _config.GetConnectionString("SQLiteFile");
        }

        public async Task<IEnumerable<RemindMeEntity>> GetAll()
        {
            await using var conn = new SQLiteConnection(_connectionString);
            await conn.OpenAsync();
            var reminders = await conn.QueryAsync<RemindMeEntity>(@"
                    select * from reminders
                ");
            return reminders;
        }

        public async Task<RemindMeEntity> Insert(RemindMeEntity reminder)
        {
            await using var conn = new SQLiteConnection(_connectionString);
            await conn.OpenAsync();
            var query = @"
                    insert into reminders
                    (ChannelId, UserId, ReminderDate, ReminderCaption, isSent)
                    values
                    (@ChannelId, @UserId, @ReminderDate, @ReminderCaption, @isSent)
                ";
            var qObj = new {
                @ChannelId = reminder.ChannelId,
                @UserId = reminder.UserId,
                @ReminderDate = reminder.ReminderDate.ToString(DateUtil.DateFormat),
                @ReminderCaption = reminder.ReminderCaption,
                @isSent = reminder.IsSent
            };
            await conn.ExecuteAsync(query, qObj);

            reminder.Id = await GetLastId("reminders");
            return reminder;
        }

        public async Task Delete(RemindMeEntity reminder)
        {
            await Delete((ulong)reminder.Id);
        }

        public async Task Delete(ulong id)
        {
            await using var conn = new SQLiteConnection(_connectionString);
            await conn.OpenAsync();
            var query = @"
                    update reminders set isSent = 1
                    where Id = @Id
                ";
            var qObj = new { @Id = id };
            await conn.ExecuteAsync(query, qObj);
        }

        public void CreateTableIfNotExists()
        {
            Log.Debug($"Creating table reminders");
            using var conn = new SQLiteConnection(_connectionString);
            conn.Open();
            var query = @"create table if not exists 
                                    reminders
                                    (Id integer not null primary key autoincrement,
                                    ChannelId unsigned big int,
                                    UserId unsigned big int,
                                    ReminderDate text,
                                    ReminderCaption text,
                                    isSent integer)";
            conn.Execute(query);
        }
        
        private async Task<long> GetLastId(string table)
        {
            await using var conn = new SQLiteConnection(_connectionString);
            await conn.OpenAsync();
            var query = "select seq from sqlite_sequence where name = @table";
            var qobj = new { @table = table };
            return await conn.QuerySingleAsync<long>(query, qobj);
        }
    }
}
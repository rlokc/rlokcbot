using System.Collections.Generic;
using System.Threading.Tasks;

namespace DiscordBot.Bot.Discord.Data
{
    public interface IRepository 
    {
        void CreateTableIfNotExists();
    }

    public interface IRepository<T> : IRepository
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> Insert(T value);
        Task Delete(ulong id);
        Task Delete(T value);
    }
}
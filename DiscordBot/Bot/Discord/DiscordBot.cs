using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using DiscordBot.Bot.Discord.Modules;
using DiscordBot.Services;
using DiscordBot.Util;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace DiscordBot.Bot.Discord
{
    public class DiscordBot 
    {

        private readonly DiscordSocketClient _client;
        private readonly CommandHandler _commandHandler;
        private readonly IConfiguration _configuration;

        private readonly RegexMatchModule _regexMatch;
        private readonly DrakeModule _drakeModule;
        private readonly CurrencyRegexModule _currencyMatcher;
        private readonly IRedditService _reddit;

        public IUser BotUser => _client?.CurrentUser;

        public DiscordBot(RegexMatchModule regexMatch,
                          CurrencyRegexModule currencyMatcher,
                          DrakeModule drakeModule,
                          IConfiguration configuration,
                          IRedditService reddit) 
        {
            _client = new DiscordSocketClient();
            var commands = new CommandService();
            _commandHandler = new CommandHandler(_client, commands);
            _configuration = configuration;
            _drakeModule = drakeModule;
            _regexMatch = regexMatch;
            _currencyMatcher = currencyMatcher;
            _reddit = reddit;
        }

        public async Task Run() 
        {
            await _client.LoginAsync(TokenType.Bot,
                                     _configuration["DiscordBotToken"]);
            await _commandHandler.InstallCommandsAsync();
            await _client.StartAsync();

            _client.MessageReceived += _regexMatch.MessageReceived;
            _client.MessageReceived += _currencyMatcher.MessageReceived;
            _client.MessageReceived += _reddit.CheckForSubreddit;
            _client.MessageReceived += _drakeModule.MessageReceived;
            _client.Log += LogDiscord;


            await Task.Delay(-1);
        }

        public bool IsReady()
        {
            return _client.ConnectionState == ConnectionState.Connected;
        }

        public Task SendMessage(string message, ulong channelId, ulong mentionUserId = 0)
        {
            var channel = _client?.GetChannel(channelId) as SocketTextChannel;
            if (channel == null)
            {
                throw new CantSendMessageException("Такого канала нет, или клиент еще не соединился");
            }
            if (mentionUserId != 0)
            {
                message = $"<@{mentionUserId}> {message}";
            }
            return channel.SendMessageAsync(message);
        }

        private Task LogDiscord(LogMessage msg) 
        {
            Log.Debug(msg.ToString());
            if (msg.Exception != null)
            {
                Log.Error(msg.Exception.ToString());
            }
            return Task.CompletedTask;
        }
    }
}
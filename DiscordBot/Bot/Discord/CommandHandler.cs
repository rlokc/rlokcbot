using System.Reflection;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using Serilog;

namespace DiscordBot.Bot.Discord
{
    public class CommandHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;

        public CommandHandler(DiscordSocketClient client, CommandService commands)
        {
            _commands = commands;
            _client = client;
        }

        public async Task InstallCommandsAsync()
        {
            _client.MessageReceived += HandleCommandAsync;

            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), Program.container);
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process command if system message
            var message = messageParam as SocketUserMessage;
            if (message == null) 
                return;

            // number to track where prefix ends and the command begins
            var argPos = 0;

            // Determine if the message is a command based on the prefix and make sure no bots trigger commands
            if ((!message.HasCharPrefix('~', ref argPos) && 
                !message.HasMentionPrefix(_client.CurrentUser, ref argPos)) ||
                message.Author.IsBot)
            {
                return;
            }

            // Create a WebSocket-based command context based on the message
            var context = new SocketCommandContext(_client, message);

            // Execute the command with the command context, along with the service provider for precondition checks
            var result = await _commands.ExecuteAsync(context: context, argPos: argPos, services: Program.container);

            if (!result.IsSuccess)
            {
                Log.Error(result.ErrorReason);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Dapper;
using Discord.Commands;
using DiscordBot.Bot.Discord.Data;
using DiscordBot.Bot.Discord.Modules;
using DiscordBot.Frontend;
using DiscordBot.Services;
using DiscordBot.Util;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using SimpleInjector;

namespace DiscordBot
{
    class Program
    {
        public static Container container;

        public static Bot.Discord.DiscordBot BotInstance { get; private set; }

        public static async Task Main(string[] args)
        {
            ConfigureLogger();
            InitDI();
            InitDB();
            InitDapperUtil();
            BotInstance = container.GetInstance<Bot.Discord.DiscordBot>();
            await Task.WhenAll(
                BotInstance.Run()
            );
            Log.CloseAndFlush();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseUrls("http://localhost:5000");
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseConfiguration(container.GetInstance<IConfiguration>());
                });
        }

        public static void ConfigureLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Async(a => a.Console())
                .WriteTo.Async(a => a.File("logs/log-.txt", rollingInterval: RollingInterval.Day))
                .MinimumLevel.Debug()
                .CreateLogger();
        }

        public static void InitDI()
        {
            container = new Container();
            container.Options.ResolveUnregisteredConcreteTypes = true;

            var configuration = InitConfig();

            container.RegisterInstance<IConfiguration>(configuration);

            var assembly = typeof(Program).Assembly;
            LoadImplementationsFromNamespace(container, assembly, typeof(IRandomService).Namespace);
            container.Register(typeof(IRepository<>), typeof(IRepository<>).Assembly, Lifestyle.Singleton);
            container.Register<Bot.Discord.DiscordBot>();
            container.Register<RegexMatchModule>();
            container.Register<CurrencyModule>();
            container.Register<CurrencyRegexModule>();

            // LoadImplementationsFromNamespace(container, Assembly.GetExecutingAssembly())
            container.Verify();
        }

        private static IConfiguration InitConfig()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";
            var configBuilder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env}.json")
                .AddJsonFile("secrets.json", optional: true)
                .AddEnvironmentVariables("DiscordBot_");

            var configuration = configBuilder.Build();

            return configuration;
        }

        private static void InitDB()
        {
            var config = container.GetInstance<IConfiguration>();
            var cs = config.GetConnectionString("SQLiteFile");

            SQLUtil.CreateDBIfNotExists(cs);

            var repTypes = container.GetCurrentRegistrations()
                .Where(r => r.ServiceType.GetInterfaces().Contains(typeof(IRepository)))
                .Select(r => r.Registration.ImplementationType);
            var repositories = repTypes.Select(t => container.GetInstance(t));
            
            foreach (IRepository rep in repositories)
            {
                rep.CreateTableIfNotExists();
            }
        }

        private static void InitDapperUtil()
        {
            SqlMapper.AddTypeHandler(new SqLiteDateTimeHandler());
            SqlMapper.AddTypeHandler(new SqLiteBoolHandler());
            SqlMapper.AddTypeHandler(new SqLiteRegexHandler());
        }

        private static List<Type> SingletonInterfaces = new List<Type>
        {
            typeof(IRandomService),
            typeof(IReminderService),
        };

        private static List<Type> GlobalBlackList = new List<Type>
        {
            typeof(ModuleBase),
            typeof(IRepository<>),
        };

        /// <summary>
        /// Загружает в контейнер службы из заданного неймспейса ns в сборке assembly, игнорируя классы из черного списка
        /// </summary>
        /// <param name="container"></param>
        /// <param name="assembly"></param>
        /// <param name="ns"></param>
        private static void LoadImplementationsFromNamespace(Container container,
                                                             Assembly assembly,
                                                             string ns,
                                                             IEnumerable<Type> blackList = null)
        {
            var registrations = assembly.GetExportedTypes()
                .Where(type => type.GetInterfaces().Any())
                .Where(type => type.Namespace.Contains(ns))
                .Where(type => !type.IsEnum)
                .Where(type => !blackList?.Contains(type) ?? true)
                .Where(type => !GlobalBlackList.Contains(type))
                .Select(type => new {
                    Service = type.GetInterfaces().Single(),
                    Implementation = type
                });

            foreach (var reg in registrations)
            {
                if (SingletonInterfaces.Contains(reg.Service) || SingletonInterfaces.Contains(reg.Implementation))
                    container.Register(reg.Service, reg.Implementation, Lifestyle.Singleton);
                else
                    container.Register(reg.Service, reg.Implementation, Lifestyle.Transient);
            }
        }

        /// <summary>
        /// Загружает в контейнер из сборки assembly все классы, реализующие какие-либо интерфейсы
        /// TODO: регистрация нескольких интерфейсов из одного класса, пока он регистрирует только первый
        /// </summary>
        /// <param name="container"></param>
        /// <param name="assembly"></param>
        private static void LoadImplementationsFromAssembly(Container container, Assembly assembly, IEnumerable<Type> blackList = null)
        {
            var registrations = assembly.GetExportedTypes()
                .Where(type => type.GetInterfaces().Any())
                .Where(type => !type.IsEnum)
                .Where(type => !blackList?.Contains(type) ?? true)
                .Where(type => !GlobalBlackList.Contains(type))
                .Select(type => new {
                    Service = type.GetInterfaces().Single(),
                    Implementation = type
                });

            foreach (var reg in registrations)
            {
                if (SingletonInterfaces.Contains(reg.Service) || SingletonInterfaces.Contains(reg.Implementation))
                    container.Register(reg.Service, reg.Implementation, Lifestyle.Singleton);
                else
                    container.Register(reg.Service, reg.Implementation, Lifestyle.Transient);
            }
        }
    }
}

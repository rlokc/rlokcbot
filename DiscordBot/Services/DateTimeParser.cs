using System;
using System.Text.RegularExpressions;
using DiscordBot.Util;
using Serilog;

namespace DiscordBot.Services
{
    public interface IDateTimeParser
    {
        DateTime Parse(string value, bool addYearIfOld = true, bool setDefaultTime = true);
    }

    public class DateTimeParser : IDateTimeParser
    {
        private static TimeSpan DefaultTimeOffset = TimeSpan.FromHours(20);
        // Regex for cases like "15 мюля 15:00"
        private readonly Regex _datetimeRegex = new(@"(\d+\s+[a-zа-я]+)\s*(\d+:\d+)\s*");
        private readonly Regex _datetimeyearRegex = new(@"(\d+\s+[a-zа-я]+\s+\d{4})\s*(\d+:\d+)\s*");
        private readonly Regex _yearRegex = new(@"\d{4}");

        public Func<DateTime> NowProvider { get; set; } = () => DateTime.UtcNow;

        public DateTime Parse(string value, bool addYearIfOld = true, 
                              bool setDefaultTime = true)
        {
            DateTime dateTime;
            value = value.ToLower();
            // Заранее проверяем на варианты вида "15 декабря (2021) 15:00" потому что они плохо парсятся DateTime.Parse
            var dtRegMatch = _datetimeRegex.Match(value);
            var dtyRegMatch = _datetimeyearRegex.Match(value);
            if (dtRegMatch.Success || dtyRegMatch.Success)
            {
                var match = (dtRegMatch.Success) ? dtRegMatch : dtyRegMatch;
                var d1 = DateTime.Parse(match.Groups[1].ToString(), DateUtil.DATELOCALE);
                var d2 = DateTime.Parse(match.Groups[2].ToString(), DateUtil.DATELOCALE);
                dateTime = d1.AddHours(d2.Hour).AddMinutes(d2.Minute);
            }
            else
            {
                try
                {
                    dateTime = DateTime.Parse(value, DateUtil.DATELOCALE);
                }
                catch
                {
                    dateTime = TryToRecoverDate(value);
                }         
            }
            // If there is no year in the date, set the year to the now year if (!_yearRegex.IsMatch(value))
            if (!_yearRegex.IsMatch(value))
            {
                dateTime = dateTime.AddYears(NowProvider().Year - dateTime.Year);
            }
            // Если дата получилась меньше сегодняшней, то добавляем год
            if (dateTime.Date < NowProvider().Date && addYearIfOld)
            {
                dateTime = dateTime.AddYears(NowProvider().Year - dateTime.Year + 1);
            }
            // Если точное время не задали, то ставим заданное по умолчанию
            if (dateTime.Hour == 0 && dateTime.Minute == 0 && setDefaultTime)
            {
                dateTime = dateTime.Add(DefaultTimeOffset);
            }
            Log.Debug($"{dateTime.ToString(DateUtil.DATELOCALE)}");
            dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified);
            dateTime = TimeZoneInfo.ConvertTimeToUtc(dateTime, DateUtil.MoscowTimeZone);

            return dateTime;
        }

        private DateTime TryToRecoverDate(string value)
        {
            DateTime? dateTime = null;
            if (value.Contains("завтра"))
            {
                dateTime = NowProvider().Date.AddDays(1);
                value = value.Replace("завтра", "");
            }
            else if (value.Contains("сегодня"))
            {
                dateTime = NowProvider().Date;
                value = value.Replace("сегодня", "");
            }
            value = value.Trim();

            // Теперь пытаемся вытащить оставшееся время, если оно есть
            if (!string.IsNullOrWhiteSpace(value))
            {
                try 
                {
                    var addTime = DateTime.Parse(value, DateUtil.DATELOCALE);
                    dateTime = dateTime.Value.AddHours(addTime.Hour).AddMinutes(addTime.Minute);
                }
                catch {}
            }

            // Есть еще вариант, когда пишут 15 июля 16:00, и тогда парсинг обваливается, обрабатываем этот вариант тоже

            if (dateTime != null)
            {
                return dateTime.Value;
            }
            throw new InvalidOperationException("Не смог определить время");
        }
    }
}
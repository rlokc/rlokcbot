using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DiscordBot.Services
{
    public interface ICurrencyExchange
    {
        Task<IDictionary<string, decimal>> GetRatesAsync();
        string BaseCurrency { get; }
    }

    public class FixerResponse 
    {
        // public bool Success { get; set; }
        public ulong Timestamp { get; set; }
        public string Base { get; set; }
        // public string Date { get; set; }
        public IDictionary<string, decimal> Rates { get; set; }
    }

    public class CurrencyExchange : ICurrencyExchange
    {

        private readonly string _fixerToken;
        public string BaseCurrency { get; private set; }

        private DateTime _lastExchangeFetch = DateTime.MinValue;
        private IDictionary<string, decimal> _lastFetchedRates;
        private readonly TimeSpan _exchangeInterval = TimeSpan.FromHours(1);
        
        public CurrencyExchange(IConfiguration configuration)
        {
            _fixerToken = configuration["CurrencyExchangeToken"] ?? throw new ArgumentNullException("CurrencyExchangeToken");
            BaseCurrency = "USD";
        }

        public async Task<IDictionary<string, decimal>> GetRatesAsync()
        {
            if (DateTime.Now - _lastExchangeFetch <= _exchangeInterval) return _lastFetchedRates;
            var response = await Task.Run(GetRawExchangeAsync);
            var apiResponse = GetExchangeData(response);
            BaseCurrency = apiResponse.Base;
            _lastFetchedRates = apiResponse.Rates;
            _lastExchangeFetch = DateTime.Now;
            return _lastFetchedRates;
        }

        private FixerResponse GetExchangeData(string data) 
        {
            return JsonConvert.DeserializeObject<FixerResponse>(data);
        }

        private async Task<string> GetRawExchangeAsync()
        {
            var uri = $"https://openexchangerates.org/api/latest.json?app_id={_fixerToken}";
            var request = WebRequest.Create(uri);

            using var response = await request.GetResponseAsync();
            await using var stream = response.GetResponseStream();
            if (stream == null) throw new NullReferenceException("Got empty stream in response");
            using var reader = new StreamReader(stream);
            return await reader.ReadToEndAsync();
        }
    }
}
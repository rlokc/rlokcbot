using System;

namespace DiscordBot.Services
{
    public interface IRandomService 
    {
        Random Random { get; }
    }

    public class RandomService : IRandomService
    {

        public Random Random { get; private set; }
        public RandomService()
        {
            Random = new Random();
        }
    }
}
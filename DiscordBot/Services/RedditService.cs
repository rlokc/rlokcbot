using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace DiscordBot.Services
{
    public interface IRedditService
    {
        Task CheckForSubreddit(SocketMessage message);
    }

    public class RedditService : IRedditService
    {
        private static Regex subredditRegex = new Regex(@"\b(?:\/)?r\/(\w+)");
        private static Regex linkRegex = new Regex(@"\b(?:\/)?r\/(\w+)\/");
        private static Regex redditLinkRegex = new Regex(@"reddit.com\b(?:\/)?r\/(\w+)\/");

        private static string redditString = "https://www.reddit.com/r/";
        public async Task CheckForSubreddit(SocketMessage message)
        {
            if (message.Author.IsBot)
            {
                return;
            }
            var match = subredditRegex.Match(message.Content);
            if (match.Success)
            {
                if (redditLinkRegex.IsMatch(message.Content) || linkRegex.IsMatch(message.Content))
                {
                    return;
                }
                var subreddit = match.Groups[1].ToString();
                await message.Channel.SendMessageAsync($"{redditString}{subreddit}");
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DiscordBot.Bot.Discord.Data;
using DiscordBot.Bot.Discord.Data.Entities;
using DiscordBot.Util;
using Serilog;

namespace DiscordBot.Services
{
    public interface IReminderService
    {
        List<RemindMeEntity> Reminders { get; }
        Task AddReminder(RemindMeEntity reminder);
    }

    public class ReminderService : IReminderService
    {
        private Bot.Discord.DiscordBot Bot => Program.BotInstance;
        private readonly IRepository<RemindMeEntity> _db;

        private readonly Guid uid = Guid.NewGuid();


        public List<RemindMeEntity> Reminders { get; set; }


        private TimeSpan pollingInterval = TimeSpan.FromSeconds(5);
        
        public ReminderService(IRepository<RemindMeEntity> db)
        {
            _db = db;
            Task.Run(() => Init());
            // Init().GetAwaiter().GetResult();
        }


        private async Task Init()
        {
            Log.Debug("Получаю активные уведомления");
            Reminders = (await _db.GetAll()).Where(r => !r.IsSent).ToList();
            foreach (var r in Reminders)
            {
                Log.Debug($"{r.ReminderDate}:{r.ReminderCaption}");
            }
            while (true)
            {
                await PollReminders();
                await Task.Delay(pollingInterval);
            }

        }

        private async Task PollReminders()
        {
            var elapsedReminders = Reminders.Where(r => r.ReminderDate <= DateTime.UtcNow);
            foreach(var r in elapsedReminders.ToList())
            {
                try 
                {
                    await SendReminder(r);
                }
                catch (CantSendMessageException ex)
                {
                    Log.Debug($"Не смог послать уведомление {r.Id}({r.ReminderCaption}): {ex.ToString()}");
                    break;
                }
                Reminders.Remove(r);
                await _db.Delete(r);
            }
        }

        private async Task SendReminder(RemindMeEntity reminder)
        {
            Log.Debug($"Посылаю уведомление {reminder.Id}: {reminder.ReminderCaption}");
            while (!Bot?.IsReady() ?? false)
            {
                Log.Debug("Бот не присоединен, пытаюсь отослать заново через 15 секунд");
                await Task.Delay(15000);
            }
            await Bot?.SendMessage(reminder.ReminderCaption, reminder.ChannelId, reminder.UserId);
        }

        public async Task AddReminder(RemindMeEntity reminder)
        {
            Log.Debug($"Добавляю новое уведомление {reminder.ReminderCaption} на {reminder.ReminderDate.ToString(DateUtil.DATELOCALE)}");
            try 
            {
                reminder = await _db.Insert(reminder);
                Reminders.Add(reminder);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }
        }
    }
}
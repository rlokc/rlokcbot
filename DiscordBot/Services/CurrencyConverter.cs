using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DiscordBot.Bot.Discord.Data;
using DiscordBot.Bot.Discord.Data.Entities;

namespace DiscordBot.Services
{
    public interface ICurrencyConverter
    {
        Task<IDictionary<string, decimal>> Convert(ulong guildId, string currency, decimal amount);
        Task<IDictionary<string, decimal>> Convert(ulong guildId, string queryString);
    }

    public class CurrencyConverter : ICurrencyConverter
    {
        private readonly IEnumerable<string> Blocklist = new string[]
        {
            "FPS",
            "RAM",
            "GMT",
            "UTC",
            "MSK",
            "AND",
            "NEW",
            "AMD",
            "PRO",
            "MAX"
        };

        private readonly IDictionary<string, decimal> MultiplierDict = new Dictionary<string, decimal>()
        {
            {"m", (decimal)Math.Pow(10, 6)},
            {"м", (decimal)Math.Pow(10, 6)},
            {"k", (decimal)Math.Pow(10, 3)},
            {"к", (decimal)Math.Pow(10, 3)},
        };

        private readonly ICurrencyExchange _exchange;
        private readonly IRepository<CurrencyConvertorList> _repository;

        public CurrencyConverter(ICurrencyExchange exchange, IRepository<CurrencyConvertorList> repository)
        {
            _exchange = exchange ?? throw new ArgumentNullException(nameof(exchange));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<IDictionary<string, decimal>> Convert(ulong guildId, string queryString)
        {
            var values = queryString.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            if (values.Length != 2) 
            {
                throw new InvalidOperationException($"Неправильное количество аргументов.");
            }
            var valueStr = values[0].Replace(',', '.').ToLower();
            var multiplier = 1.0m;

            foreach (var c in MultiplierDict.Keys)
            {
                if(valueStr.Contains(c))
                {
                    multiplier = MultiplierDict[c];
                    valueStr = valueStr.Replace(c, "");
                    break;
                }
            }

            if (!decimal.TryParse(valueStr, out var amount)) 
            {
                throw new InvalidOperationException($"Не смог преобразовать {values[0]} в число");
            }
            amount *= multiplier;
            var currency = values[1].ToUpper();
            return await Convert(guildId, currency, amount);
        }

        private async Task<IEnumerable<string>> GetCurrenciesToConvertTo(ulong guildId)
        {
            var currencyLists = await _repository.GetAll();
            var currencyList = currencyLists.FirstOrDefault(l => l.GuildId == guildId);
            if (currencyList == null)
            {
                currencyList = new CurrencyConvertorList(guildId, CurrencyConvertorList.DefaulCurrencies);
                await _repository.Insert(currencyList);
            }

            return currencyList.Currencies;
        }

        public async Task<IDictionary<string, decimal>> Convert(ulong guildId, string currency, decimal ammount)
        {
            if (Blocklist.Contains(currency))
            {
                return null;
            }

            var rates = await _exchange.GetRatesAsync();
            var ammountInBase = ConvertToBase(ammount, currency, rates);

            var resDict = new Dictionary<string, decimal>
            {
                [_exchange.BaseCurrency] = ammountInBase,
                [currency] = ammount
            };
            var currencyList = await GetCurrenciesToConvertTo(guildId);
            currencyList = currencyList.Where(cur => rates.ContainsKey(cur));
            foreach (var curr in currencyList.Where(c => c != currency && c != _exchange.BaseCurrency))
            {
                resDict[curr] = ConvertFromBase(ammountInBase, curr, rates);
            }

            return resDict;

            // var resBuilder = new StringBuilder();
            // foreach(var kvp in res_dict.OrderBy(kvp => kvp.Key))
            // {
            //     resBuilder.AppendLine($"{kvp.Key}: {kvp.Value.ToString("N2")}");
            // }

            // return resBuilder.ToString();
        }

        public decimal ConvertToBase(decimal ammount, string currency, IDictionary<string, decimal> rates)
        {
            return ammount / rates[currency];
        }

        public decimal ConvertFromBase(decimal ammount, string to, IDictionary<string, decimal> rates)
        {
            return ammount * rates[to];
        }
    }
}
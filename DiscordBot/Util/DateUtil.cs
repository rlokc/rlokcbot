using System;
using System.Globalization;

namespace DiscordBot.Util
{
    public static class DateUtil
    {
        public const string DateFormat = "yyyy-MM-dd HH:mm:ss";
        public static readonly CultureInfo DATELOCALE = new CultureInfo("ru-RU");

        public static readonly TimeZoneInfo MoscowTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Moscow");
        public static TimeZoneInfo UtcTimeZone = TimeZoneInfo.Utc;

    }
}
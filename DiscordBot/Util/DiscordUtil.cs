using System;
using Discord;
using Discord.WebSocket;

namespace DiscordBot.Util;

public static class DiscordUtil
{
    public static ulong GetGuildId(SocketMessage msg)
    {
        var user = (IGuildUser)msg.Author;
        return user.GuildId;
    }
}
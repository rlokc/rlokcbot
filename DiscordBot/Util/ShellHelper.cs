using System.Diagnostics;
using System.Threading.Tasks;

namespace DiscordBot.Util
{
    public static class ShellHelper
    {
        public static async Task<string> BashAsync(this string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");

            var process = new Process();
            process.StartInfo = new ProcessStartInfo
            {
                FileName = "/bin/bash",
                Arguments = $"-c \"{escapedArgs}\"",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            };

            process.Start();
            var res = await process.StandardOutput.ReadToEndAsync();

            process.WaitForExit();
            return res;
        }
    }
}
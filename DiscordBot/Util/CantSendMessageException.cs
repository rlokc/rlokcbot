using System;

namespace DiscordBot.Util
{
    public class CantSendMessageException : Exception
    {
        public CantSendMessageException(string message) : base(message) {}
        public CantSendMessageException()
        {}

    }
}
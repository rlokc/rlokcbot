using System;
using System.Data;
using System.Text.RegularExpressions;
using Dapper;
using DiscordBot.Bot.Discord.Data.Entities;

namespace DiscordBot.Util
{
    public class SqLiteDateTimeHandler : SqlMapper.TypeHandler<DateTime>
    {
        public override DateTime Parse(object value)
        {
            return DateTime.ParseExact(value.ToString() ?? string.Empty, DateUtil.DateFormat, null);
        }

        public override void SetValue(IDbDataParameter parameter, DateTime value)
        {
            parameter.Value = value.ToString(DateUtil.DateFormat);
        }
    }

    public class SqLiteBoolHandler : SqlMapper.TypeHandler<bool>
    {
        public override bool Parse(object value)
        {
            return (long)value != 0;
        }

        public override void SetValue(IDbDataParameter parameter, bool value)
        {
            parameter.Value = value ? 1 : 0;
        }
    }

    public class SqLiteRegexHandler : SqlMapper.TypeHandler<Regex>
    {
        public override Regex Parse(object value)
        {
            return new Regex((string)value, RegexMatch.DefaultOptions);
        }

        public override void SetValue(IDbDataParameter parameter, Regex value)
        {
            parameter.Value = value.ToString();
        }
    }
}